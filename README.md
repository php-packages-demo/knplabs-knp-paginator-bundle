# [knplabs](https://phppackages.org/s/knplabs)/[knp-paginator-bundle](https://phppackages.org/p/knplabs/knp-paginator-bundle)

[**knplabs/knp-paginator-bundle**](https://packagist.org/packages/knplabs/knp-paginator-bundle)
[![rank](https://pages-proxy.gitlab.io/phppackages.org/knplabs/knp-paginator-bundle/rank.svg)](http://phppackages.org/p/knplabs/knp-paginator-bundle)
SEO friendly Symfony paginator to sort and paginate http://knplabs.com/en/blog/knp-paginator-reborn

Unofficial Demo and Howto

## Other pager
* [paginator](https://phppackages.org/s/paginator) (PHPPackages.org)
* nette/utils
* [white-october/pagerfanta-bundle](https://phppackages.org/p/white-october/pagerfanta-bundle) <br/>
  ([php-packages-demo/white-october-pagerfanta-bundle](https://gitlab.com/php-packages-demo/white-october-pagerfanta-bundle))
* illuminate/pagination
  * https://laravel.com/api/5.3/Illuminate/Pagination.html
  * https://laravel-guide.readthedocs.io/en/latest/pagination/
* knplabs/knp-components
* laminas/laminas-paginator

## Unofficial documentation
* [*How to install the KnpPaginatorBundle to paginate Doctrine queries in Symfony 4*](https://ourcodeworld.com/articles/read/802/how-to-install-the-knppaginatorbundle-to-paginate-doctrine-queries-in-symfony-4)
